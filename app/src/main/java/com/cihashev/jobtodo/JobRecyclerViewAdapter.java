package com.cihashev.jobtodo;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * {@link RecyclerView.Adapter} that can display a {@link Job}.
 */
public class JobRecyclerViewAdapter extends RecyclerView.Adapter<JobRecyclerViewAdapter.ViewHolder> {

    private final JobGroup jobGroup;

    public JobRecyclerViewAdapter(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final JobStorage jobStorage = JobStorage.getInstance();
        holder.jobName.setText(jobStorage.getJob(position, jobGroup).getName());
        holder.jobDoneBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!jobStorage.isJobDone(position, jobGroup)) {
                    jobStorage.setJobDone(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, getItemCount());
                }
            }
        });
        holder.deleteJobBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                jobStorage.removeJob(position, jobGroup);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
            }
        });
    }

    @Override
    public int getItemCount() {
        return JobStorage.getInstance().size(jobGroup);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView jobName;
        public final ImageButton jobDoneBtn;
        public final ImageButton deleteJobBtn;

        public ViewHolder(View view) {
            super(view);
            jobName = view.findViewById(R.id.job_name);
            jobDoneBtn = view.findViewById(R.id.job_done_btn);
            deleteJobBtn = view.findViewById(R.id.delete_job_btn);
        }
    }
}