package com.cihashev.jobtodo;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A fragment representing a list of Items.
 */
public class JobFragment extends Fragment {

    private static final String ARG_JOB_GROUP = "jobGroup";

    private JobGroup jobGroup;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public JobFragment() {
    }

    public static JobFragment newInstance(JobGroup jobGroup) {
        JobFragment fragment = new JobFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_JOB_GROUP, jobGroup);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            jobGroup = (JobGroup) getArguments().getSerializable(ARG_JOB_GROUP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_list, container, false);

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        // TODO: add here some items to test
        recyclerView.setAdapter(new JobRecyclerViewAdapter(jobGroup));
        return view;
    }
}